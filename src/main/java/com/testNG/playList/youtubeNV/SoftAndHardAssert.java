package com.testNG.playList.youtubeNV;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class SoftAndHardAssert {
	
	SoftAssert softAssert=new SoftAssert();
	/*
	 * Soft Assert vs Hard Assert in TestNG:
	 * 
	 * +hard assertion: hard validation: if a hard assertion is getting failed:
	 * immediately test case will me marked as failed and test case will be
	 * terminated.
	 * 
	 * +soft assertion : soft validation: if a soft assertion is getting failed:
	 * test case will not be marked as passed as well as next lines will be executed
	 * 
	 * +assertAll() : is used to mark the test case as failed, if any soft assertion
	 * is getting failed
	 */
	
	@Test
	public void test1()
	{
		System.out.println("Open browser");
		System.out.println("Login page");
		
		Assert.assertEquals(false, true);//HardAssert
		
		System.out.println("Login title");
		System.out.println("DealsPage");
		softAssert.assertEquals(false, true);//softAssert
		System.out.println("Deals Page title");
		softAssert.assertAll();
		
		
	}
	
	@Test
	public void test2()
	{
		
	}
	
	@AfterClass
	public void tearDown()
	{
		
	}
	

}
