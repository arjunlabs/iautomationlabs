package com.qa.pages;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.qa.core.util.WebDriverFactory;

public class googleTest {
	
	WebDriver driver;
	
	@Test
	public void launchGoogle()
	{
		WebDriverFactory factory = new WebDriverFactory();
		factory.init();
		factory.launchUrl("https://www.google.com");
		factory.getPageTitle();
		factory.closeBrowser();
		
		
	}

}
