package com.qa.labs.seleniumNewLabs;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class SendKeysCharSequence {

	public static void main(String[] args) {

		WebDriverManager.chromedriver().setup();
		WebDriver driver=new ChromeDriver();
		
		
		driver.get("https://app.hubspot.com");
		driver.manage().timeouts().implicitlyWait(5000, TimeUnit.SECONDS);
		
		WebElement email =driver.findElement(By.xpath("//input[@id='username']"));
		//1. String
	//	email.sendKeys("arjunkumayan18@gmail.com");
		
		//2. using StringBuilder
		
		//StringBuilder username=new StringBuilder().append("Naveen").append(" ").append(" Automation").append("Labs").append("Selenium");
		
		
		//email.sendKeys(username);
		
		// 3.StringBuffer
		
		StringBuffer username=new StringBuffer().append("Naveen").append(" ").append(" Automation").append("Labs").append("Selenium");
		
	//	email.sendKeys(username);
		
		//4. StringBuilder, StringBuffer,String,Keys
		StringBuilder username1=new StringBuilder().append("Naveen").append(" ").append(" Automation").append("Labs").append("Selenium");
		String space=" ";
		
		StringBuffer username2=new StringBuffer().append("Test").append(" WebDriver");
				
		String author="Arjun Singh";
		
		email.sendKeys(username1,space,username2,author,Keys.TAB);
		
		
	}

}
