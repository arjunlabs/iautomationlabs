package com.labs.learnProgramming;

public class FloatAndDoubleChallenge {

	public static void main(String[] args) {

		int myIntValue=5/3;
		float myFloatValue=5f/3f;
		Double mydoubleValue=5d/3d;
		Double mydoubleValue1=5.00/3.00;
		
		System.out.println(myIntValue);
		System.out.println(myFloatValue);
		System.out.println(mydoubleValue);
		System.out.println(mydoubleValue1);
	}

}
