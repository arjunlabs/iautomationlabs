package com.labs.learnProgramming;

public class PrimitiveTypeChallenge {

	public static void main(String[] args) {

		byte byteValue=10;
		short shortValue=20;
		int intValue=50;
		
		long longValue=500000L;
		
		long longTotal=500000L+10L+(byteValue+shortValue+intValue);
		System.out.println(longTotal);
		
		short shortTotal=(short) ((1000+10)*(byteValue+shortValue+intValue));
		System.out.println(shortTotal);
		
		//Single precision occupies - 32 bits
		// double - 64bits
	}

}
