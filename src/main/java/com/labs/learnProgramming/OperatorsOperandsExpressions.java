package com.labs.learnProgramming;

public class OperatorsOperandsExpressions {
	
	 static int test=100;
	 //test=300;
	
	public void m1()
	{
		test=200;
		System.out.println(test);
	}

	public static void main(String[] args) {
		
		OperatorsOperandsExpressions mp=new OperatorsOperandsExpressions();
		mp.m1();

		int result = 1 + 2; // 1+2=3
		System.out.println("1+2=" + result);

		int previousResult = result;
		System.out.println("PreviousResult: " + previousResult);

		result = result - 1;
		System.out.println("3-1" + result);
		System.out.println("PreviousResult: " + previousResult);

		result = result * 10;
		System.out.println("2*10: " + result);

		result = result / 5;
		System.out.println("20/5:" + result);

		result = result % 3;

		System.out.println("4%3: " + result);

		result++;
		System.out.println(result);
		result--;
		System.out.println(result);

		result += 2;
		System.out.println(result);

		result *= 3;
		System.out.println(result);

		result /= 2;

		System.out.println(result);

		boolean isAlien = false;
		if (isAlien == false)
		{
			System.out.println(" it is not an alien!");
		    System.out.println("And I am scared of Aliens");
		}
		System.out.println("______________________________________________________________");
		
		// AND Operator
		int topScore=1000;
		int secondTopScore=200;
		if((topScore>secondTopScore) && (topScore<2000) )
		{
			System.out.println("Greater than scond top score and less than 2000");
		}
		System.out.println("______________________________________________________________");
		int marksInMaths=50;
		int marksInPhysics=100;
		
		if(topScore>secondTopScore || topScore >2000)
		{
			System.out.println(" SecondTop score is less than top score and greater than 2000");
		}
		
		System.out.println("______________________________________________________________");

		
		// OR Operator
		if(marksInMaths >10 || marksInPhysics >150)
		{
			System.out.println("marks in maths is greater than 10 and marks in physics is also greater than 50");
		}
		
		System.out.println("______________________________________________________________");
		
		// = Assignment operator
		// == equals to operator
		int newValue=50;
		if(newValue==50)
		{
			System.out.println("This is true");
		}
		
		System.out.println("______________________________________________________________");
		
		boolean isCar=false;
		if(isCar!=true)
		{
			System.out.println("This is not supposed to happen");
		}
		
		if(!isCar)
		{
			System.out.println("this is true");
		}
		
		
		
		boolean wasCar=isCar?true:false;
		if(wasCar==false)
		{
			System.out.println("wasCar is false");
		}
		
		boolean isACar=true;
		boolean wasACar=isACar?true:false;
		if(wasACar==true)
		{
			System.out.println("wasACar is false");
		}
		
	}

}
