package com.labs.learnProgramming;

public class DataTypeRecap {

	public static void main(String[] args) {
		
		// 8 primitive data types in java
        //Byte
		// Short
		// char
		// int
		// long
		// float
		// double
		// float
		// boolean
		
		// String is not a primitive data types, its a class
		// String - Sequence of character
		
		String myString="This is String";
		System.out.println("myString is equal to: "+myString);
		
		myString=myString+"and this is more";
		System.out.println("myString is equal to: "+myString);
		myString=myString+"\u00A9 2019";
		System.out.println("myString is equal to: "+myString);
		
		String numberString="250.55";
		numberString=numberString+"49.55";
		System.out.println(numberString);
		
		String lastString="10";
		int myInt=10;
		lastString=lastString+myInt;
		System.out.println("last string is equal to: "+lastString);
		
		double doubleNumber=120.47d;
		lastString=lastString+doubleNumber;
		
		System.out.println(lastString);
		
		// String is immutable 
		// A new string is created
		
		
		
	}

}
