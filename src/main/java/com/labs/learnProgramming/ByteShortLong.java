package com.labs.learnProgramming;

public class ByteShortLong {

public static void main(String[] args)
{
		
		  int myValue=1000000000;
		  System.out.println(myValue);
		 
	
	int myIntMinValue=Integer.MIN_VALUE;
	int myIntmaxValue=Integer.MAX_VALUE;
	
	System.out.println("Integer minimum value: "+myIntMinValue);
	System.out.println("Integer maximum value: "+myIntmaxValue);
	System.out.println("Busted maximum value: "+(myIntmaxValue+1));
	System.out.println("Busted minimum value: "+(myIntMinValue-1));
	
	//Underflow in the case of minimum
	//overflow in the case of maximum
	
	//int = 4 byte
	//int myMaxIntest=2147483648; // out of range for integer
	int myMaxIntest=2_147_483_647;
	System.out.println(myMaxIntest);
	
	byte byteMinValue=Byte.MIN_VALUE;
	byte byteMaxValue=Byte.MAX_VALUE;
	System.out.println(byteMinValue +" and "+byteMaxValue);
	
	//short= 2 byte
	short shortMinValue=Short.MIN_VALUE;
	short shortMaxValue=Short.MAX_VALUE;
	System.out.println("Short minimum"+shortMinValue );
	System.out.println("Short maximum"+shortMaxValue);
	
	
	
	// byte = 8bits
	byte byteMnValue=Byte.MIN_VALUE;
	byte byteMxValue=Byte.MAX_VALUE;
	
	System.out.println("Byte Minimum value: "+byteMnValue);
	System.out.println("Byte maximum value: "+byteMxValue);
	

	// long - 8bytes
	long myLongMinValue=Long.MIN_VALUE;
	long myLongMaxValue=Long.MAX_VALUE;
	System.out.println("Long minimum value: "+myLongMinValue);
	System.out.println("Long maximium value"+myLongMaxValue);
	
	long myLongValue=100L;
	long myLongv=2_147_483_647L;
	System.out.println(myLongValue);
	System.out.println(myLongv);
	
	/**
	Integer minimum value: -2147483648
	Integer maximum value: 2147483647
	Busted maximum value: -2147483648
	Busted minimum value: 2147483647
	2147483647
	-128 and 127
	Short minimum-32768
	Short maximum32767
	Byte Minimum value: -128
	Byte maximum value: 127
	Long minimum value: -9223372036854775808
	Long maximium value9223372036854775807
	
	**/
	
	int total=(myIntMinValue/2);
	System.out.println(total);
	
	byte myMinByteValue=(byte) (byteMnValue/2);
	
	System.out.println(myMinByteValue);
	
	short myMinShortValue= (short) (shortMinValue/2);
	
	System.out.println(myMinShortValue);
}
}
