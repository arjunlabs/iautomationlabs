package com.labs.learnProgramming;

public class CharAndBoolean {
	
	

	public static void main(String[] args) {

		char myChar = 'D';
		char myUnicode = '\u0044';
		System.out.println(myChar);
		System.out.println(myUnicode);

		char myCopyRightChar = '\u0049';
		System.out.println(myCopyRightChar);

		// size - 1 character only
		// 2 byte of memory
		
		// Boolean - 1 byte

		Boolean myTrueBooleanValue = true;
		Boolean myFalseBooleanValue = false;
		System.out.println(myFalseBooleanValue);

		System.out.println(myTrueBooleanValue);

		Boolean isCustomerOverTwentyOne = true;

		System.out.println(isCustomerOverTwentyOne);

	}

}
