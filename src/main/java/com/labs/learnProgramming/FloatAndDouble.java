package com.labs.learnProgramming;

public class FloatAndDouble {

	public static void main(String[] args) {

		float myFloatMinValue=Float.MIN_VALUE;
		float myFloatMaxValue=Float.MAX_VALUE;
		
		System.out.println(myFloatMinValue);
		System.out.println(myFloatMaxValue);
		
		double mydoubleMinValue=Double.MIN_VALUE;
		double mydoubleMaxValue=Double.MAX_VALUE;
		
		System.out.println(mydoubleMinValue);
		System.out.println(mydoubleMaxValue);
		
		
		float value=5.1f;
		float value1=(float)5.1;
		double test=10.34;
		
	}

}
