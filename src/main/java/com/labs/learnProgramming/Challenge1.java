package com.labs.learnProgramming;

public class Challenge1 {

	public static void main(String[] args) {

		// convert 1 pound to kilogram
		
		// Float and double are general for floating operations
		
		float f=1.5F;
		
		Double onePound=0.45359237d;
		
		Double pound=200d;
		Double convertedKilogram=pound*onePound;
		System.out.println("Converted Kilogram: "+convertedKilogram);
		
		double pi=3.1415927d;
		double anotherNumber=3_000_00.4_567_890d;
		
		System.out.println(anotherNumber);
		
	}

}
