package com.labs.stringStringStrinBufferStringBuilder;

public class StringBufferBasics {

	public static void main(String[] args) {

		// CASE -1
		StringBuffer sb=new StringBuffer("Durga");
		sb.append("software");
		System.out.println(sb);
		
		
		// CASE- 2
		StringBuffer sb1=new StringBuffer("Durga");
		StringBuffer sb2=new StringBuffer("Durga");
		
		System.out.println(sb1==sb2);
		System.out.println(sb1.equals(sb2));
	}

}
