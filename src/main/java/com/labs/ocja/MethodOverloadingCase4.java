package com.labs.ocja;

public class MethodOverloadingCase4 {
	
	public void m1(int i)
	{
	
		System.out.println("Int arg method");
	}

	public void m1(int... i)
	{
		System.out.println("Var arg method");
	}
	public static void main(String[] args) {
		MethodOverloadingCase4 mo=new MethodOverloadingCase4();
		mo.m1(10);
		mo.m1(10,20,30);
		mo.m1('a');
		// mo.m1(10.5);  C.E
		
	}

}
