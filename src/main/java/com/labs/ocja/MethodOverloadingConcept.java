package com.labs.ocja;

public class MethodOverloadingConcept {
	
	public void M1()
	{
		System.out.println("No Arg");
	}
	
	public void M1(int i)
	{
		System.out.println("int Arg");
	}
	
	public void M1(double d)
	{
		System.out.println("double Arg");
	}
	
	public static void main(String[] args) {

		
		/* Inside a class, same method name with different argument type
		 * Compile is responsible for method resolution based on class reference type.
		 * Method Resolution- which method to call
		 * Comile time polymorphism| Early Binding | Statis polymorphism
		 *    M1(int)
		 *    M1(long)
		 *    
		 *    Method name same + Argument type are different
		 *    
		 *    Return type is not part of the method overloading
		 *    Method Signature- Method name followed by argument type
	*/
		
		MethodOverloadingConcept mo=new MethodOverloadingConcept();
		mo.M1();
		mo.M1(10);
		mo.M1(10.5);
		
		
	}

}
