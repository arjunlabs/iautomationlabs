package com.labs.ocja;

public class MethodOverloadingCase2 {
	// 
	
	public void M1(Object o)
	{
		System.out.println("object arg");
	}
	
	public void M1(String s)
	{
		System.out.println("String arg");
	}

	public static void main(String[] args) {

		MethodOverloadingCase2 mc=new MethodOverloadingCase2();
		mc.M1(new Object());
		mc.M1("Arjun");
		
		mc.M1(null);
		// Null can be passed as Object and String
		
		/*  In Overloading if both child and parent classes are available then always the priority to be for Child method
		 * 
		 * Object is Parent of String class
		 * 
		 * Object
		 *   |
		 *  String
		 *  
		 *   so string class argument will get the highest priority
		 * 
		 * 
		 */
	}

}
