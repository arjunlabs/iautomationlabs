package com.labs.ocja;

public class Child extends Parent {
	
	static int x=888;
	static void display()
	{
		System.out.println("static method in child");
	}

	public static void main(String[] args) {

		
		Parent p=new Child();
		System.out.println(p.x);
        p.display();
		Parent p1=new Parent();
		System.out.println(p1.x);
		p1.display();
		
		Child c1=new Child();
		System.out.println(c1.x);
		c1.display();
	}

}
