package com.labs.ocja;

public class MethodOverridingParent {
	
	public void property()
	{
		System.out.println(" cash+gold+Land");
		
	}

	public void marry()
	{
		System.out.println("Appalamma|Subbalamma");
	}
	 // Parent class method which is overridden method called overridden method
	// Child class method which is overriding method is called overriding method

		// whatever a method parent have directly available to child class thru inheritance
		// If child class is not happy with parent class method then child class can create the same method in 
		// child class is called - Method Overriding
		
	// If the child class is not satisfied with the parent method implementation then child is allowed to redefine the method in child class
	
	
		
	

}
