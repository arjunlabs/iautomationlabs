package com.labs.ocja;

public class MethodOverloadingCase5 {
	
	public void M1(int i,float f)
	{
		System.out.println("Int,Float");
	}
	
	public void M1(float i,int f)
	{
		System.out.println("float,Int");
	}
	
	

	public static void main(String[] args) {

		MethodOverloadingCase5 mp=new MethodOverloadingCase5();
		mp.M1(10, 10.5f);
		mp.M1(10.5f, 10);
		
	//	mp.M1(10, 10);  // C.E - reference to M1 is ambigious
	//	mp.M1('a', 10); // C.E - reference to M1 is ambigious
		
	}

}
