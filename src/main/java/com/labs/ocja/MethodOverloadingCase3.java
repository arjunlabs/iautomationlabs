package com.labs.ocja;

public class MethodOverloadingCase3 {

	public void M1(String s)
	{
		System.out.println("String arg");
	}
	
	public void M1(StringBuffer sb)
	{
		System.out.println("StringBuffer arg");
	}
	
	
	public void M1(String[] args)
	{
		System.out.println("String array args");
	}
	
	public static void main(String[] args) {

		// Exact match will get highest priority
		MethodOverloadingCase3 mo=new MethodOverloadingCase3();
		mo.M1("Durga");
		mo.M1(new StringBuffer("Durga"));
		// mo.M1(null); // Compile time error
		/* There should be parent and child relation for the method to be called
		 * 
		 * If method argument passed is getting valid for both method in that case it will get the C.E
		 * Here the String and StringBuffer are both child class of object class so here the compiler is confused which method to be called
		 * C.E- The reference to M1 is ambiguous
		 * 
		 * 
		 */
		
		
	}

}
