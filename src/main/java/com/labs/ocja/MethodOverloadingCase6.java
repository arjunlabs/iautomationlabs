package com.labs.ocja;

public class MethodOverloadingCase6 {
	
	public void M1(Animal a)
	{
		System.out.println("Animal");
	}
	public void M1(Monkey m)
	{
		System.out.println("Monkey");
	}
	
	class Animal{
		
	}
	class Monkey extends Animal{
		
	}
	

	public static void main(String[] args) {

		MethodOverloadingCase6 mc=new MethodOverloadingCase6();
		//Animal a=new Animal();
		
		
	}
	
	

}
