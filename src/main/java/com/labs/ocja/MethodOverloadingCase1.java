package com.labs.ocja;

public class MethodOverloadingCase1 {
	
	// Automatic promotion
	// If while calling the method the argument type does not matched with method available then it will promote
	// to next level in call hirarchy and if next level is also not present then only will throw Compile Error.
	// 
	
	public void M1(int i)
	{
		System.out.println("Int arg");
	}
	
	public void M1(float f)
	{
		System.out.println("Float arg");
	}

	public static void main(String[] args) {
/*  In Method overloading - method resolution is takes care by compiler not by run time object
 * Compile time | Static | Early binding
 */
		
		MethodOverloadingCase1 mc=new MethodOverloadingCase1();
		mc.M1(10);
		
		mc.M1('a');  // it will promotes to next level that is int level so output to be 'Int arg' 
		
		// Call hierarchy level
		//  Byte ->Short -
		//                Int -> Long ->Float -> double
		//         Char -
		
		
		//mc.M1(10.5); // next level is not present so C.E
	}

}
