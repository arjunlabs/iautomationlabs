package com.labs.ocja;

public class MethodOverridingChild  extends MethodOverridingParent{
	
	public void marry()
	{
		System.out.println("Katrina");
	}

	public static void main(String[] args) {
		
		MethodOverridingParent mp=new MethodOverridingParent();
		mp.marry(); // Parent method 
		
	
		MethodOverridingChild mc=new MethodOverridingChild();
		mc.marry(); // Child method
		
		MethodOverridingParent mcp=new MethodOverridingChild();  // Runtime Polymorphism
		mcp.marry(); // child method
		
		// Method resolution- which method to call is choose
		// parent reference can be used to hold child class object
		// at Runtime JVM will check if the parent method is overridden in child method or not
		
		// Method resolution - is always takes care by JVM based on runtime object
		
		// Run time polymorphism | Dynamic polymorphism | Late binding
		

	}

}
