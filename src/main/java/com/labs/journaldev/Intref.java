package com.labs.journaldev;

public interface Intref {
	
	void m1();
	
	default void show()
	{
		System.out.println("Show in interface");
	}

}
