package com.labs.journaldev;

import java.util.Arrays;
import java.util.List;

public class ForEachMethod {

	public static void main(String[] args) {

		List<Integer> arrayList=Arrays.asList(10,20,30,40);
		
		/*
		 * for(int i:arrayList) { System.out.println(i); }
		 */
		arrayList.forEach(i->System.out.println(i));
	}

}
