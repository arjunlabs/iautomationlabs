package com.labs.journaldev;

import java.util.ArrayList;
import java.util.List;

public class ClassA implements Interface1,Interface2{

	public static void main(String[] args) {
		
		Interface1 objRef=new ClassA();
		Interface2 objRef1=new ClassA();
		objRef.method1();
		objRef.method2();
		ClassA.nullCheck();
		objRef1.method1();
		objRef1.method2();
		
		List<String> li=new ArrayList<>();
   li.add("A");
   li.add("b");
   li.add("c");
   li.add("d");
   System.out.println(li);
   

	}

	@Override
	public void method1() {
		System.out.println("Class A implementation");
		
	}
	
	
	/* public void method2() { System.out.println("classA"); } */
	 


	@Override
	public void method2() {
		System.out.println("classA method2");
	}

	static boolean nullCheck()
	{
		System.out.println("null check in ClassA");
		return true;
	}
	
}
