package com.labs.journaldev;

public interface Interface1 {
	
	void method1();
	
	default void method2()
	{
		System.out.println("interface1");
	}
	
	static boolean nullCheck()
	{
		System.out.println("null check in interface1");
		return true;
	}

}
