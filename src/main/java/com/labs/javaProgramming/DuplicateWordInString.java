package com.labs.javaProgramming;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class DuplicateWordInString {

	//Java is Java best language is Java
	// Java - 3
	//IS -2
	// Using hashMap
	
	public static void findDuplicateWords(String inputString)
	{
		//split the string
		String words[] = inputString.split(" ");
		 
		// create one hashmap
		
		Map<String,Integer> wordcount=new HashMap<String,Integer>();
		
		//to check each word in given array
		
		for(String word: words)
		{
			//System.out.println(word);
			// if word is present in array
			if(wordcount.containsKey(word))
			{
				//int count = wordcount.get(word);
				
				wordcount.put(word,wordcount.get(word)+1);
			}
			else
			{
				wordcount.put(word, 1);
			}
		}
		// extracting all the keys of map - wordCount
		
		
		Set<String> wordsInString= wordcount.keySet();
		
		/*
		 * System.out.println(wordsInString);
		 *  System.out.println(wordcount.entrySet());
		 */
		//System.out.println(wordcount.entrySet());
		// loop through all the words in wordCount
		for(String word :wordsInString )
		{
			if(wordcount.get(word)>1)
			{
				System.out.println(word + " : "+wordcount.get(word));
			}
		}
		
		
	}
	public static void main(String[] args) {

		
		findDuplicateWords("Java is Java but best language is Java");
		System.out.println(" ==== ");
		
		findDuplicateWords("Hey Python Python is not java but best language is Python not java");
		
			
		findDuplicateWords("100 200 100 300 400 500 500");
		
		
		
		
		
		
		
	}

}
