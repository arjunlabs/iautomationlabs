package com.labs.stringStringBufferStringBuilder;

public class StringBasic {
	
 public static void	main(String []args)
 {
	 
	 // CASE -1
	 String s=new String("Durga");
	 s.concat("soft");
	 System.out.println(s);
	 
	 
	 // CASE -2
	 String s1=new String("Durga");
	 String s2=new String("Durga");
	 
	 System.out.println(s1==s2);
	 System.out.println(s1.equals(s2)); //TRUe because meant for content comparison
	 
 }

}
