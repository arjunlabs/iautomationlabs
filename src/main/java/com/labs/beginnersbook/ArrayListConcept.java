package com.labs.beginnersbook;

import java.util.ArrayList;

public class ArrayListConcept {

	public static void main(String[] args) {

		
		ArrayList<String> alist=new ArrayList<String>();
		alist.add("Arjun");
		alist.add("Rahul");
		alist.add("shyam");
		alist.add("Mohan");
		alist.add("Ramesh");
		
		System.out.println("Displaying elements"+alist);
		System.out.println(alist.size());
		System.out.println(alist.get(1));
		
		for(int i=0;i<alist.size();i++)
		{
			System.out.println(alist.get(i));
		}
		alist.add(1, "Singh");
		System.out.println("size method"+alist.get(1));
		//alist.addAllc(queryList);
		
		ArrayList<String> queryList=new ArrayList<String>();
		
		String query1="Transaction query";
		String query2= "Payment query";
		String query3="Credit transaction";
		queryList.add(query1);
		queryList.add(query2);
		queryList.add(query3);
		
		
		System.out.println(queryList.size());
		
		for(String str:queryList)
		{
			System.out.println(str);
		}
		
	 }

}
