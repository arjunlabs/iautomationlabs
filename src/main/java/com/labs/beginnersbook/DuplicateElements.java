package com.labs.beginnersbook;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class DuplicateElements {

	public static void main(String[] args) {

		String names[]= {"Java","JavaScript","Ruby","C","Java","Python","JavaScript"};
		
		//Compare each elements
		
		for(int i=0;i<names.length;i++)
		{
			for(int j=i+1;j<names.length;j++)
			{
				if(names[i].equalsIgnoreCase(names[j]))
				{
					System.out.println("Duplicate elements is: "+names[i]);
				}
				// time complexity will be O(n2)
				
			}
		}
		
		
		//2. HashSet - is part of java collection- it stores unique values
		
		Set<String> store=new HashSet<String>();
		for(String name:names)
		{
			if(store.add(name)==false)
			{
				System.out.println("Duplicate elements is : "+name);
				
			}
			
		}
		
		System.out.println("******************");
		
		//3. Using HashMap : duplicate elements are allowed
		//String , Integer
		
		Map<String,Integer> storeMap=new HashMap<String,Integer>();
		
		for(String name:names)
		{
			Integer count=storeMap.get(name);
			if(count == null) {
				storeMap.put(name, 1);
				
			}
			
			else
			{
				storeMap.put(name,++count);
				
				}
					}
		
		//get the value from hashmap
		
		Set<Entry<String,Integer>> entrySet=storeMap.entrySet();
		
		for(Entry<String,Integer> entry:entrySet)
		{
			if(entry.getValue()>1) {
				System.out.println("duplicatae elements is: "+entry.getKey()+" value is: "+entry.getValue());
			}
		}
		
		
		
		
		
		
		
		
		
	}

}
