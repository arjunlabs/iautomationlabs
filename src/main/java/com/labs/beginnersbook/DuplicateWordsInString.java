package com.labs.beginnersbook;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class DuplicateWordsInString {

	public static void main(String[] args) {

		//Hey java is Java best language is java
		//java = 3
		//is =3
		
		
		finDuplicateWords("Hey java is Java best language is java python is python");
		
	}

	
	public static void finDuplicateWords(String inputString)
	{
//split
		String words[]=inputString.split(" ");
		
		//create HashMap
		
		Map<String,Integer> wordCount=new HashMap<String,Integer>();
		
		//to check the each word in given array
		for(String word:words)
		{
			// if word is present in array
			if(wordCount.containsKey(word))
			{
				wordCount.put(word.toLowerCase(), wordCount.get(word)+1);
			}
			else
			{
				wordCount.put(word, 1);
			}
		}
		
		
		// extracting all the keys of map
		
	Set<String> wordsInString	=wordCount.keySet();
	//loop all the words in wordsCount
		
		for(String word:wordsInString)
		{
			if(wordCount.get(word)>1)
			{
				System.out.println(word+ " :"+wordCount.get(word));
			}
		}
		
		
		
	}
}
