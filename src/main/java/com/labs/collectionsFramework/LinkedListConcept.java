package com.labs.collectionsFramework;

import java.util.LinkedList;

public class LinkedListConcept {

	
	public static void main(String[] args)
	{
		
		LinkedList link=new LinkedList();
				
				link.add("Arjun");
		        link.add("Singh");
		        
		        System.out.println(link.get(0));
		        System.out.println(link.getFirst());
		        
		        link.addFirst(10);
		        System.out.println(link.getFirst());
		        
		        link.addLast("kumayan");
		        System.out.println(link.getLast());
		        
		        link.add("Hello");
		        
		        System.out.println(link.getLast());

		        link.removeLast();
		        System.out.println(link.getLast());
		        
	}
}
