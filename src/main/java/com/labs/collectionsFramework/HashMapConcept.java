package com.labs.collectionsFramework;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class HashMapConcept {

	public static void main(String[] args) {

		
		HashMap hm=new HashMap();
		hm.put("Arjun", 100);
		hm.put("Priyank", 200);
		hm.put("Kuldeep", 300);
		
		System.out.println(hm);
		// Object will be stored based on hashcode of keys
		
		System.out.println(hm.put("Chiranjivi", 1000));
		System.out.println(hm.put("Chiranjivi", 2000));
		
		
	System.out.println(hm.keySet());
	
	Set s=hm.keySet();
	System.out.println(s);
	
	System.out.println(hm.containsKey("Arjun"));
		
	
	Set s1=hm.entrySet();
	System.out.println(s1);
	
	Iterator itr=s1.iterator();
	
	
	while(itr.hasNext())
	{
	Map.Entry m1=(Map.Entry) itr.next();
	
System.out.println(m1.getKey()+" "+m1.getValue());

if(m1.getKey().equals("Arjun"))
{
	m1.setValue(2000);
}
	}
	
	System.out.println(hm);
	
	
	}

}
