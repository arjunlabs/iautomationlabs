package com.labs.collectionsFramework;

import java.util.TreeMap;

public class TreeMapConcept {

	public static void main(String[] args) {

		
		TreeMap tm=new TreeMap();

	//	tm.put(null,"BBB" ); // NPE 
		
		tm.put(130, null);
		tm.put(1340, null);
		tm.put(100, "zzz");
		tm.put(103, "YYY");
		tm.put(104, "AAA");
		//tm.put("ABC", "NNN"); // CCE
		
		System.out.println(tm);
	}

}
