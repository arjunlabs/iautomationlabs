package com.labs.collectionsFramework;

import java.util.ArrayList;
import java.util.Iterator;

public class IteratorConcept {

	public static void main(String[] args) {

		
		ArrayList al=new ArrayList();
		
		for(int i=0;i<10;i++)
		{
			al.add(i);
		}
		System.out.println(al);
		
		Iterator itr =al.iterator();
		
		while(itr.hasNext())
		{
			Integer value=(Integer)itr.next();
			if(value%2==0)
			{
				System.out.println(value);
			}
			else
			{
			itr.remove();
			}
		}
		
		
	}

}
