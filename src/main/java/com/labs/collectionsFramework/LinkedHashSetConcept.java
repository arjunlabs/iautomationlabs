package com.labs.collectionsFramework;

import java.util.HashSet;
import java.util.Iterator;

public class LinkedHashSetConcept {

	public static void main(String[] args) {

		HashSet h=new HashSet();
		//Default capacity is 16 and default load factor is 0.75 %
		
		
		h.add("A");
		h.add("B");
		h.add(null);
		h.add("c");
		h.add("10");
		
		System.out.println(h.add("A"));
		System.out.println(h); // insertion order- based on hashcode of object
		
		Iterator itr=h.iterator();
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
		
	}

}
