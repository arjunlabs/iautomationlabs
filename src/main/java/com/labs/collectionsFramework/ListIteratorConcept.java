package com.labs.collectionsFramework;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;

public class ListIteratorConcept {

	public static void main(String[] args) {

		LinkedList al=new LinkedList();
		al.add("Bala");
		al.add("tala");
		al.add("rala");
		al.add("krishna");
		al.add("guru");
		System.out.println(al);
				
		/*
		 * for(int i=0;i<10;i++) { al.add(i); }
		 */
		System.out.println(al);
		
		ListIterator ltr=al.listIterator();
		while(ltr.hasNext())
		{
		String str=(String)ltr.next();
			if(str.equals("tala"))
			{
				ltr.remove();
			}
			else if(str.equals("rala"))
			{
				ltr.add("arju");
			}
			else if(str.equals("guru"))
			{
				ltr.set("suru");
			}
				
		}
		
		System.out.println(al);
	}

}
