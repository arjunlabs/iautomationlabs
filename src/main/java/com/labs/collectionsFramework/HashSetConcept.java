package com.labs.collectionsFramework;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;

public class HashSetConcept {

	public static void main(String[] args) {

		LinkedHashSet h=new LinkedHashSet();
		//Default capacity is 16 and default load factor is 0.75 %
		
		
		h.add("A");
		h.add("B");
		h.add(null);
		h.add("c");
		h.add("10");
		
		System.out.println(h.add("A"));
		System.out.println(h); // insertion order- is preserved in LinkedHashset
		
		Iterator itr=h.iterator();
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
		
	}

}
