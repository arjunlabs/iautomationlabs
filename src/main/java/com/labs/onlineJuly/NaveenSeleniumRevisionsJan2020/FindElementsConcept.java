package com.labs.onlineJuly.NaveenSeleniumRevisionsJan2020;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FindElementsConcept {
	static By createLink = By.xpath("//a[contains(text(),'Create a new account')]");
	static By foregetLink = By.xpath("//a[contains(text(),'Forgot Password?')]");

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "D:\\Work_Soft\\Drivers\\chromedriver_win32\\chromedriver.exe");

		WebDriver driver = new ChromeDriver();
		driver.get("https://mail.rediff.com/cgi-bin/login.cgi");
		List<WebElement> linklist = driver.findElements(By.tagName("a"));
		System.out.println("Total links count: " + linklist.size());
		for (int i = 0; i < linklist.size(); i++) {
			String text = linklist.get(i).getText();
			System.out.println(text);

		}

		WebElement createElement = driver.findElement(createLink);
		createElement.click();
		driver.navigate().back();
		createElement = driver.findElement(createLink);
		createElement.click();

	}

}
