package com.labs.onlineJuly.NaveenSeleniumRevisionsJan2020;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebDriverBasic {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver",
				"D:\\\\Work_Soft\\\\Drivers\\\\chromedriver_win32\\\\chromedriver.exe");

		WebDriver driver = new ChromeDriver();
		driver.get("http://www.google.com");
		String title = driver.getTitle();
		if (title.equalsIgnoreCase("Google")) {
			System.out.println("Correct Title");
		} else {
			System.out.println("In Correct");
		}
		
		driver.quit();
		
		System.out.println(driver.getTitle());

	}

}
