package com.labs.onlineJuly.NaveenSeleniumRevisionsJan2020;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Util {

	WebDriver driver;

	/**
	 * This is used to get the alert Text
	 * 
	 * @param driver
	 * @return
	 */

	public static String getAlertText(WebDriver driver) {
		Alert alert = driver.switchTo().alert();
		String text = alert.getText();
		alert.accept();
		return text;

	}

	/**
	 * This is used to launch the URL
	 * 
	 * @param driver
	 * @param url
	 */

	public static void launchUrl(WebDriver driver, String url) {
		driver.get(url);

	}

	/**
	 * This method is used to launch the browser
	 * 
	 * @param driver
	 * @param browserName
	 * @return
	 */

	public static WebDriver launchBrowser(WebDriver driver, String browserName) {

		if (browserName.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver",
					"D:\\Work_Soft\\Drivers\\chromedriver_win32\\chromedriver.exe");
			driver = new ChromeDriver();
		} else if (browserName.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver",
					"D:\\Work_Soft\\Drivers\\FireFox_driver_win64\\geckodriver.exe");
			driver = new ChromeDriver();
		} else {
			System.out.println("browser name is not correct" + browserName);

		}
		return driver;

	}

	/**
	 * get the element
	 * 
	 * @param driver
	 * @param locator
	 * @return
	 */

	public static WebElement getElement(WebDriver driver, By locator) {
		WebElement element = driver.findElement(locator);
		return element;
	}

	/**
	 * quit the browser
	 * @param driver
	 */
	public static void closeBrowser(WebDriver driver)
	{
		driver.quit();
	}
	/**
	 * click the element
	 * @param driver
	 * @param locator
	 */
	public static void doClick(WebDriver driver,By locator)
	{
		driver.findElement(locator).click();
	}
	/**
	 * get the page title
	 * @param driver
	 * @return
	 */
	public static String getPageTitle(WebDriver driver)
	{
		String title=driver.getTitle();
		return title;
	}
	
}
