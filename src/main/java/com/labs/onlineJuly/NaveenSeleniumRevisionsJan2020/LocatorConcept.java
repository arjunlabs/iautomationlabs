package com.labs.onlineJuly.NaveenSeleniumRevisionsJan2020;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LocatorConcept {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "D:\\Work_Soft\\\\Drivers\\chromedriver_win32\\chromedriver.exe");

		WebDriver driver = new ChromeDriver();
		 driver.get("https://app.hubspot.com");
		//driver.get("http://classic.crmpro.com");

		Thread.sleep(5000);

		// HTML DOM
		// 1. id
		By username = By.id("username");
		By password = By.id("password");
		By loginButton = By.id("loginBtn");

		/*
		 * Util.getElement(driver, username).sendKeys("arjunkumayan18@gmail.com");
		 * Util.getElement(driver, password).sendKeys("Defence@5050");
		 * Util.getElement(driver, loginButton).click();
		 */
		// 1- First approaches
		// WebElement username = driver.findElement(By.id("username"));
		// username.sendKeys("arjunkumayan18@gmail.com");

		// 2-second approaches
		// driver.findElement(By.id("username")).sendKeys("arjunkumayan18@gmail.com");

		// WebElement userElement=driver.findElement(username);
		// userElement.sendKeys("arjunkumayan18@gmail.com");

		// WebElement password = driver.findElement(By.id("password"));
		// password.sendKeys("Defence@5050");

		// WebElement loginButton = driver.findElement(By.id("loginBtn"));
		// loginButton.click();

		// 2. Name

		// driver.findElement(By.name("username")).sendKeys("arjunkumyan18@gmail.com");
		//WebElement element = driver.findElement(By.name("username"));
		//element.sendKeys("arjunkumayan18@gmail.com");
		
		/*
		 * By username1=By.name("username"); By password1=By.name("password");
		 * 
		 * WebElement element=Util.getElement(driver, username1);
		 * element.sendKeys("arjunkumayan18@gmail.com"); WebElement
		 * element1=Util.getElement(driver, password1);
		 * element1.sendKeys("Defence@5050");
		 * 
		 * Util.getElement(driver, By.xpath("//input[@value='Login']")).click();
		 */
		
		//3. xpath
		
		//driver.findElement(By.xpath("")).sendKeys("arjunkumayan18@gmail.com");
		
		//By username2=By.xpath("//*[@value='Login']");
		//Util.getElement(driver, username2).sendKeys("arjunkumayan18@gmail.com");
		
		//4. Css selector
		/*
		 * By username3=By.cssSelector("input[name='username']");
		 * Util.getElement(driver, username3).sendKeys("test@gmail.com"); By
		 * password3=By.cssSelector("input[name='password']"); Util.getElement(driver,
		 * password3).sendKeys("test@gmail.com");
		 * 
		 * //5. class name By loginButton1=By.className("input-group-btn");
		 * Util.getElement(driver, loginButton1).sendKeys("test@gmail.com");
		 */
		
		
		//6. link Text: only for links
		
		//By forgetLink=By.linkText("Forgot my password");
		//Util.getElement(driver, forgetLink).click();
		
		//7. Partial link text: only for links
		By forgetLink=By.partialLinkText("Forgot my");
		Util.getElement(driver, forgetLink).click();
		
		//8. Tag name
		
		//By username5=By.tagName("");
		
	}

}
