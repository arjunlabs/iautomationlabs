package com.labs.onlineJuly.SeleniumSessions;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class PageLoad {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "D:\\Work_Soft\\Drivers\\chromedriver_win32\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		
		//driver.get("https://app.hubspot.com/login");
		
		driver.navigate().to("https://app.hubspot.com/login");
		driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
		
		System.out.println("abc");
		
		driver.getTitle();
		
	}

}
