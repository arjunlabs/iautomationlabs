package com.labs.onlineJuly.javaSessions;

import com.labs.onlineJuly.util.*;



public class StudentLib {

	public static void main(String[] args) {
		
		  Library lib = new Library();
		  
		  System.out.println(lib.totalNumberOfBooks());
		  
		  int booksCount = lib.getSubjectBooks("Physics");
		  System.out.println("total physics books are: "+ booksCount);
		 
	}

}
