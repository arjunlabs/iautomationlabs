package com.labs.onlineJuly.javaSessions;


public class ExceptionHandling {

	// Unwanted error which is terminating your program
	public static void main(String[] args) {

		System.out.println("A");

		try {
			int i = 9 / 0;
		} 
		
		catch (Throwable e) {
			System.out.println("some exception occured");
			e.printStackTrace();
		}
		finally
		{
			System.out.println("Finally block");
		}

		System.out.println("B");

	}

}
