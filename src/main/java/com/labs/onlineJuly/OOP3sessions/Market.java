package com.labs.onlineJuly.OOP3sessions;

public interface Market {

	public void kitchen();
	
	default void list()
	{
		System.out.println("");
	}
}
