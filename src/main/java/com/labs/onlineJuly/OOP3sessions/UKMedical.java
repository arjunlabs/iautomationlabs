package com.labs.onlineJuly.OOP3sessions;

public interface UKMedical {
	
	public void physioServices();
	
	public void dentalServices();
	
	public void medicalInsurance();

	public void doctorsInternship(int doctors);

	public void doctorsInternship(int doctors, int nurse);


}
