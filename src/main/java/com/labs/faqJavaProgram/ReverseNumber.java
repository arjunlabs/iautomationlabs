package com.labs.faqJavaProgram;

import java.util.Scanner;

public class ReverseNumber {

	public static void main(String[] args) {

		/*
		 * Scanner sc = new Scanner(System.in);
		 * 
		 * System.out.println("Enter the number"); int number = sc.nextInt();
		 */
		
		// 1. using Algorithm
		
		// 12345
		/*
		 *  number = 12345
		 *   find the remainder = number%10; store it to somewhere
		 *   remove the last number = number/10 - it will give you last number then again take modulo and 
		 *  
		 * 
		 * 
		 */
		
		int num =12345;
		int sum =0;
		
		while(num>0)
		{
			int remainder = num%10;
			sum = sum*10 + remainder;
			num = num/10;
		}
		
		System.out.println(sum);
		
		
	
		
		
			
		
		
	}

}
