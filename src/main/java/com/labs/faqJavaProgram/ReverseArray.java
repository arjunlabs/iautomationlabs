package com.labs.faqJavaProgram;

public class ReverseArray {
	
	public static void reverseArray(int ar[])
	{
		int len = ar.length;
		int start = 0;
		int end = len-1;
		int temp;
		while(start<end)
		{
			temp= ar[start];
			ar[start]=ar[end];
			ar[end] = temp;
		
			start++;
			end--;
		}
	}
	
	public static void printArray(int arr[])
	{
		
		for(int i=0 ;i<arr.length ;i++)
		{
			System.out.print(arr[i]+" ");
		}
	}
		

	public static void main(String[] args) {

		int arr[] = {1,2,3,4,5,6};
		printArray(arr);
		reverseArray(arr);
		System.out.println("After reverse");
		printArray(arr);
		
	}
		
	}

