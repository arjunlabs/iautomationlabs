package com.labs.faqJavaProgram;

public class Swap_Number {
	

	public static void main(String[] args) {
		int a=10;
		int b=20;
		
		// Logic 1
		System.out.println("Before Swapping values:"+a+ " and "+b);
		
		int temp = a;
		a=b;
		b=temp;
				
		System.out.println("After Swapping values:"+a+ " and "+b);

		//Logic 2
		
		a=a+b;  // 30
		b=a-b;  // 30-20 = 10
		a=a-b; //30 - 10=20
		System.out.println("After Swapping values:"+a+ " and "+b);
		
		
		
	}

}
