package com.labs.faqJavaProgram;

public class ReverseString {
	
	// Using character Array
	public void usingCharacterArray(String str)
	{
		int len = str.length();
		
		char[] ch=str.toCharArray();
		
		String rev="";
		
		for(int i=len-1; i>=0 ; i--)
		{
			rev= rev+ch[i];
		}
		
		System.out.println(rev);
	}

	// Using String + Concatation
	public void usingStringConacatation(String str)
	{
		int len = str.length();
		
		String valueToBeConvert = str;
		String rev = "";
		for(int i=len-1 ; i>=0 ; i--)
		{
			rev= rev + valueToBeConvert.charAt(i);
		}
		System.out.println("Reverse String is: "+rev);
	}
	
	public void usingStringBuffer(String str)
	{
		StringBuffer sb= new StringBuffer(str);
		System.out.println(sb.reverse());
		
	}
	public static void main(String[] args) {
		
		
		ReverseString rs = new ReverseString();
		rs.usingCharacterArray("ARJUN");
		
		rs.usingStringConacatation("ARJUN SINGH");
		rs.usingStringBuffer("Kumayan");
	}

}
