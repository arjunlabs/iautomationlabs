package com.labs.faqJavaProgram;

public class ReverseAString {
	
	public static void reversingString(String str)
	{
		
		char[] ch = str.toCharArray();
		for(char ch1:ch)
		{
			System.out.print(ch1+" ");
		}
		
		System.out.println();
		
		int len= str.length();
		int start = 0;
		int end = len-1;
		char temp;
		while(start<end)
		{
			temp = ch[start];
			ch[start]= ch[end];
			ch[end] = temp;
			
			start ++;
			end --;
		
		}
		for(int i=0; i<ch.length ; i++)
		{
			System.out.print(ch[i]+ " ");
		}
		
		
		
		
	}
	
	public static void reversingStringWord(String str)
	{
		
		String[] str1 = str.split(" ");
		for(String value:str1)
		{
			System.out.print(value+" ");
		}
		
		System.out.println();
		
		int len= str1.length;
		int start = 0;
		int end = len-1;
		String temp;
		while(start<end)
		{
			temp = str1[start];
			str1[start]= str1[end];
			str1[end] = temp;
			
			start ++;
			end --;
		
		}
		for(int i=0; i<str1.length ; i++)
		{
			System.out.print(str1[i]+ " ");
		}
		
		
		
		
	}
	public static void printArray(String str)
	{
	char[] str1 =	str.toCharArray();
		for(int i=0; i<str1.length; i++)
		{
			System.out.print(str1[i] + " ");
		}
	}

	public static void main(String[] args) {

		
		String str1[] = {"Arjun","Singh","Kumayan"};
		
		String str ="PROGRAMMING AND LANGUAGE";
		//printArray(str);
		System.out.println();
		reversingString(str);
		//printArray(str);
		
		System.out.println("-----------");
		reversingStringWord(str);
		
		
	}

}
