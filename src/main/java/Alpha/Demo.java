package Alpha;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Demo {
	
	private static Logger log=LogManager.getLogger(Demo.class.getName());
	
	public static void main(String args[])
	{
		// debug- to show the in progress information
		// error- to show the error message but it will not terminate the program
		// fatal- it will show the error but will terminate the program
		//Use log. Error() to log when elements are not displayed in the page or if any validations fail

		//Use Log. Debug()

		//When each Selenium action is performed like click, SendKeys, getText()

		//Use log.info()

		//When operation is successfully completed ex: After loading page, or after any successful validations

		//It’s just counterpart to log. Error()

//
//		log.trace("Trace Message!");
//	      log.debug("Debug Message!");
//	      log.info("Info Message!");
//	      log.warn("Warn Message!");
//	      log.error("Error Message!");
//	      log.fatal("Fatal Message!");
		log.debug("I am debugging");
		if(2<0)
		{
			log.debug("Object is present");
		}
		else
		{
		log.error(" Object is not present");
	}
		
		if(5>4)
		{
			log.info("object is present");
			log.error("object is not present");
			log.fatal("This is fatal");
		}
	}

}
