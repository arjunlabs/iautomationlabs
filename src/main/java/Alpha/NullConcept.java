package Alpha;

public class NullConcept {

	static int a;

	
	static Object obj;
	static String str;
	static NullConcept objNull;
	
	//5. Static vs NonStatic wrt null
	public static void sum()
	{
		System.out.println(" sum ");
	}
	
	public void div()
	{
		System.out.println(" div");
	}
	public static void main(String[] args) {
		
		//1. case sensitive
		 //Object obj=null;
		
		//2. default reference value will be null
		/*
		 * System.out.println(NullConcept.a); System.out.println(NullConcept.obj);
		 * System.out.println(NullConcept.str); System.out.println(NullConcept.objNull);
		 */
		//3. Null value can't be assigned to primitive data type
	//	int b=null; // 
		// only wrapper types
		
		//Integer k=null;
		
		//4. instanceOf
		/*
		 * Integer i=null; Integer j=10;
		 * 
		 * System.out.println(i instanceof Integer); System.out.println(j instanceof
		 * Integer);
		 */
		
		//5. Static vs NonStatic wrt null
		//NullConcept obj=null;
		//  obj.sum();
		// NullConcept.sum();
	   
		//obj.div();
		
		// 6. == and !=
		
		//System.out.println(null==null);
		//System.out.println(null!=null);
		
		// Default value of String
		
		String str=null;
		Integer itr=null;
		Double d=null;
		// Olnly non primitive and object class or class can be assigned to null
		
		String s1= (String) null;
		System.out.println(s1);
		System.out.println(s1+ " 123");
		//System.out.println(s1.length()); // NPE
				// On null reference no action can't be performed
		Integer i2=	(Integer) null;
	    Double d2=(Double) null;
	    
	    

	}

}
