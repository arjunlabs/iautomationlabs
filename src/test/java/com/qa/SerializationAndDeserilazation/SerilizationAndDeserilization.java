package com.qa.SerializationAndDeserilazation;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

// Serilization - The process of converting java object to File
// Deserilization - the process of reading state of object from a file 

class Test implements Serializable
{
	int i=10,j=20;

	public int getI() {
		return i;
	}

	public void setI(int i) {
		this.i = i;
	}

	public int getJ() {
		return j;
	}

	public void setJ(int j) {
		this.j = j;
	}

	public Test(int i, int j) {
		this.i = i;
		this.j = j;
	}
	
}


public class SerilizationAndDeserilization {
	
	

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		
		Test t1=new Test(10,20);
		
		// Serilization
		
		FileOutputStream fos=new FileOutputStream("test.txt");
		
		ObjectOutputStream oos=new ObjectOutputStream(fos);
		
		oos.writeObject(t1);
		
		// Deserilization
		
		FileInputStream fis=new FileInputStream("test.txt");
		ObjectInputStream ois=new ObjectInputStream(fis);
		Test t2=  (Test) ois.readObject();
		
		System.out.println(t2.i+" "+t2.j);

	}

}
