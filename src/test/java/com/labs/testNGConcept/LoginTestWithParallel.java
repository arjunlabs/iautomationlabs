package com.labs.testNGConcept;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class LoginTestWithParallel {
	
WebDriver driver;
	
	
	By username=By.id("username");
	By password=By.id("password");
	By login=By.id("loginBtn");
	By signUp=By.linkText("Sign up");
	
	By homePageHeader=By.xpath("//h1[@class='private-page__title']");
	 // please run /LabsAutomation/src/testresults/resources/testrunners/Hubspot_regression.xml
	@BeforeMethod
	@Parameters({"url","browser"})
	public void setUp(String url,String browser) throws Exception
	{
		if(browser.equalsIgnoreCase("chrome"))
		{
			WebDriverManager.chromedriver().setup();
			driver=new ChromeDriver();
		}
		else if(browser.equalsIgnoreCase("ff"))
		{
			WebDriverManager.firefoxdriver().setup();
			driver=new FirefoxDriver();
		}
		else
		{
			System.out.println(" ");
			throw new Exception("NoSuchBrowserException");
		}
		System.out.println(" Before method ");
		WebDriverManager.chromedriver().setup();
		driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get(url);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		
				
	}
	@Test(priority=1)
	public void signUpLinkTest()
	{
		System.out.println("test method 1");
	Boolean result=	driver.findElement(signUp).isDisplayed();
	
	Assert.assertTrue(result);
	}
	
		
	@Test(priority=2, enabled=false)
	public void pageTitleTest()
	{
		String title = driver.getTitle();
		System.out.println("Page title is: "+title);
		//doImplicitWait(driver,20L);
		Assert.assertEquals(title, "HubSpot Login");
	}
	@Test(priority=3, enabled= true)
	public void loginTest()
	{
		driver.findElement(username).sendKeys("arjunkumayan18@gmail.com");
		driver.findElement(password).sendKeys("Defence@5050");
		driver.findElement(login).click();
		//doImplicitWait(driver,40L);
		String homePageHeaderVal=driver.findElement(homePageHeader).getText();
	    Assert.assertEquals(homePageHeaderVal, "Sales Dashboard");
	    Assert.assertTrue(homePageHeaderVal.contains("Sales Dashboard"));
	    
	}

	@AfterMethod
	//@AfterTest
	public void tearDown()
	{
		System.out.println("after method");
		driver.quit();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
