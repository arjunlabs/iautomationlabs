package com.labs.testNGConcept;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class LoginPageCase2 {
	
	
WebDriver driver;
	
	@BeforeTest
	public void setUp()
	{
		WebDriverManager.chromedriver().setup();
		driver=new ChromeDriver();
	//	driver.manage().window().fullscreen();
	 driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		//driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.get("https://app.hubspot.com");
		
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.titleContains("HubSpot Login"));

		
	}
	
	@Test
	public void loginPageTest()
	{
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.titleContains("HubSpot Login"));

		String title = driver.getTitle();
		System.out.println(" page title is : " + title);

		if (title.equals("HubSpot Login")) {
			System.out.println("Pass");
		} else {
			System.out.println("FAIL");
		}
	}
	
	@Test
	public void signUpLinkTest()
	{
	boolean flag =	driver.findElement(By.linkText("Sign up")).isDisplayed();
	Assert.assertTrue(flag);
	}
	
	@AfterTest
	public void tearDown()
	{
		driver.quit();
	}


}
