package com.labs.testNGConcept;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class DataProviderConcept {
	
	@DataProvider
	public Object[][] getData()
	{
		// Combination of different data
		
		// Username, Password
		Object data[][]=new Object[3][2];
		//1st set
		data[0][0]="firstusername";
		data[0][1]="password1";
		
		//2nd set
		data[1][0]="secondusername";
		data[1][1]="password2";
		
		//3rd set
		
		data[2][0]="thirdusername";
		data[2][1]="password3";
		return data;
					
		
	}
	
	@Test(dataProvider= "getData")
	public void userRegistration(String username,String password)
	{
		
		System.out.println(username);
		System.out.println(password);
	}

}
