package com.labs.testNGConcept;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class TestNGAnnotationsConcept {
	
	// Before  - BS,BT,BC,BM
	// Test   - Test
	// After  - AS, AT,AC,AM
	
	/**
	 * BS - BeforeSuite
	 * BT - BeforeTest
	 * BC - BeforeClass
	 * BM - BeforeMethod
	 * T1 - TestMethod
	 * AM - AfterMethod
	 * BM - BeforeMethod
	 * T2 - TestMethod
	 * AM - AfterMethod
	 * 
	 * AC - AfterClass
	 * AT - AfterTest
	 * AS - AfterSuite
	 * 
	 * 
	 */
	// If you have one field that is dynamic then how will you locate the element
	// using js
	// go to console and write
	// document.queryselector("*[type='email']").getAttribute("id");
	WebDriver driver;
	
	
	By username=By.id("username");
	By password=By.id("password");
	By login=By.id("loginBtn");
	By signUp=By.linkText("Sign up");
	
	By homePageHeader=By.xpath("//h1[@class='private-page__title']");
	

	public void doImplicitWait(WebDriver driver, Long time)
	{
		driver.manage().timeouts().implicitlyWait(time, TimeUnit.SECONDS);
	}
	@BeforeSuite
	public void beforeSuite()
	{
	
		System.out.println(" before suite -- prepare the data");
		
	}
	
	@BeforeTest
	public void beforeTest()
	{
		System.out.println(" before test -- DB connection");
	}
	
	@BeforeClass
	public void beforeClass()
	{
		System.out.println(" before class --  get the value from DB");
	}
	
	@BeforeMethod
	//@BeforeTest
	public void setUp()
	{
		System.out.println(" Before method ");
		WebDriverManager.chromedriver().setup();
		driver=new ChromeDriver();
		driver.get("https://app.hubspot.com");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
				
	}
	
	@Test(priority=1)
	public void signUpLinkTest()
	{
		System.out.println("test method 1");
	Boolean result=	driver.findElement(signUp).isDisplayed();
	
	Assert.assertTrue(result);
	}
	
	@Test
	public void test2()
	{
		System.out.println("test2");
		Assert.assertTrue(false);	
	}
	
	@Test(priority=2, enabled=false)
	public void pageTitleTest()
	{
		String title = driver.getTitle();
		System.out.println("Page title is: "+title);
		doImplicitWait(driver,20L);
		Assert.assertEquals(title, "HubSpot Login");
	}
	
	@Test(priority=3, enabled= false)
	public void loginTest()
	{
		driver.findElement(username).sendKeys("arjunkumayan18@gmail.com");
		driver.findElement(password).sendKeys("Defence@5050");
		driver.findElement(login).click();
		doImplicitWait(driver,40L);
		String homePageHeaderVal=driver.findElement(homePageHeader).getText();
	    Assert.assertEquals(homePageHeaderVal, "Sales Dashboard");
	    Assert.assertTrue(homePageHeaderVal.contains("Sales Dashboard"));
	    
	}
	
	@Test(enabled=false)
	public void homePageTest()
	{
		driver.findElement(username).sendKeys("arjunkumayan18@gmail.com");
		driver.findElement(password).sendKeys("Defence@5050");
		driver.findElement(login).click();
		doImplicitWait(driver,40L);
	}
	
	@AfterMethod
	//@AfterTest
	public void tearDown()
	{
		System.out.println("after method");
		driver.quit();
	}
	
	@AfterClass
	public void afterClass()
	{
		System.out.println(" after class -- delete the user");
	}
	
	@AfterTest
	public void afterTest()
	{
		System.out.println(" after test-- disconnect the db connection");
		
	}

	
	@AfterSuite
	public void afteSuite()
	{
		System.out.println(" after suite -- delete test data");
	}
}
