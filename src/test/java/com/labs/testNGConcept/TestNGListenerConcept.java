package com.labs.testNGConcept;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class TestNGListenerConcept implements ITestListener {

	@Override
	public void onTestStart(ITestResult result) {
		System.out.println(" onTestStart");
	//	System.out.println(result.getTestName());
		//System.out.println(result.getStatus());
		System.out.println(result.getName());
		
	}

	@Override
	public void onTestSuccess(ITestResult result) {
		System.out.println(" onTestSuccess");
		System.out.println(result.getTestName());
		System.out.println(result.getStatus());
		System.out.println(result.getName());
	}

	@Override
	public void onTestFailure(ITestResult result) {
		System.out.println(" onTestFailure");
		System.out.println(result.getTestName());
		System.out.println(result.getStatus());
		System.out.println(result.getName());
		
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		System.out.println(result.getTestName());
		System.out.println(result.getStatus());
		System.out.println(result.getName());
		
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
	
		
	}

	@Override
	public void onStart(ITestContext context) {
		
		
	}

	@Override
	public void onFinish(ITestContext context) {
		
		
	}

}
