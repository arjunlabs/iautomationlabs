package com.labs.testNGConcept;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class SignUpTest {
	
	WebDriver driver;
	By createAccountLinkText=By.xpath("//div[@class='layout-header']/h4");
	By firstNameLabel=By.xpath("//label[@id='UIFormControl-label-8']");
	By lastNameLabel=By.xpath("//label[@id='UIFormControl-label-10']");
	
	By signUp=By.xpath("//div[@class='signup-link']/a");	
	By termServiceLink=By.xpath("//a[text()='HubSpot Customer Terms of Service.']");
	// please run the following path for parameter concept
	///LabsAutomation/src/testresults/resources/testrunners/Hubspot_regression.xml
	
	@BeforeTest
	@Parameters({"url","browser"})
	public void setUp(String url,String browser) throws Exception
	{
		System.out.println(url);
		
		if(browser.equalsIgnoreCase("chrome"))
		{
			WebDriverManager.chromedriver().setup();
			driver=new ChromeDriver();
		}
		else if(browser.equalsIgnoreCase("ff"))
		{
			WebDriverManager.firefoxdriver().setup();
			driver=new FirefoxDriver();
		}
		else
		{
			System.out.println(" ");
			throw new Exception("NoSuchBrowserException");
		}
		
		driver.manage().window().maximize();
		driver.get("https://app.hubspot.com");
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		
	}
	
	@Test(priority=1)
	public void veriCreatAccountTest()
	{
		Assert.assertTrue(driver.findElement(signUp).isDisplayed());
		driver.findElement(signUp).click();
		
	}
	
	@Test(priority=2)
	public void verifyAccountNameTest()
	{
		Assert.assertTrue(driver.findElement(createAccountLinkText).isDisplayed());
	}
	
	
	@AfterTest
	public void tearDown()
	{
		driver.quit();
	}
	
	
	
	
	
	
	
	
	
	

}
