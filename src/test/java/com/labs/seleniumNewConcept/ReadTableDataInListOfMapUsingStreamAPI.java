package com.labs.seleniumNewConcept;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.http.HeaderElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class ReadTableDataInListOfMapUsingStreamAPI {

	
	
	@Test
	public void readTableDataInListOfMap()
	{
		WebDriverManager.chromedriver().setup();
		
		WebDriver driver=new ChromeDriver();
		
		String fileURL=System.getProperty("user.dir");
		System.out.println(fileURL);
		
		driver.get(fileURL+"/src/test/resources/Registration.html");
		
		// Each row will be a key value pair. So we will use LinkedHashMap so that the order can be retained
		// All map will be added to list
		
		List<LinkedHashMap<String,String>> allTableData=new ArrayList<LinkedHashMap<String,String>>();
		
		// Get total headers of table using stream apis,
		
		String rowLoc="//table[@class='tg']/tbody/tr";
		
		List<String> headers=driver.findElements(By.xpath("//table[@class='tg']/tbody/tr//th")).stream().map(headerEle ->headerEle.getText()).collect(Collectors.toList());
		
		System.out.println(headers);
		
		
		
	}
}
