
package com.labs.deseriliazePojo;

import java.util.List;

public class GoRest {

    private Meta _meta;
    
    private List<Result> result = null;

    public Meta getMeta() {
        return _meta;
    }

    public void setMeta(Meta meta) {
        this._meta = meta;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

}
