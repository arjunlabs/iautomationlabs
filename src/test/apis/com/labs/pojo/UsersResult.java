
package com.labs.pojo;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UsersResult {

 
    private Meta _meta;
   
    private List<Result> result = null;

    public Meta getMeta() {
        return _meta;
    }

    public void setMeta(Meta meta) {
        this._meta = meta;
    }

    public List<Result> getResult() {
        return result;
    }

    public void setResult(List<Result> result) {
        this.result = result;
    }

}
