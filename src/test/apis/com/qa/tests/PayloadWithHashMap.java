package com.qa.tests;

import java.util.HashMap;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class PayloadWithHashMap {
	
	
	
	@Test
	public void enterPayload()
	{
		HashMap<String, String> map = new HashMap<String,String>();
		map.put("username", "arjunkumayan18@gmail.com");
		map.put("password", "Defence@5050");
		
		RequestSpecification  request = RestAssured.given().log().all();
		request.body(map);
		
		Response response = request.post("");
		System.out.println(response.prettyPrint());
		
	}

}
