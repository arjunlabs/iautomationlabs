package com.qa.tests;

import javax.security.auth.Subject;

import org.testng.annotations.Test;

import com.labs.pojo.Result;
import com.labs.pojo.UsersResult;

import static io.restassured.RestAssured.*; 
import static io.restassured.matcher.RestAssuredMatchers.*;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.matcher.RestAssuredMatchers.*;
public class ExtractRequestJsonArrayResponseToArray {

	
	// This will help to get the json response into array 
	// suppose we have json response and response is also coming in array but how to store the same response into array, will se it here
	
	
	@Test
	public void getJsonResponseInArray()
	{
        RestAssured.baseURI="https://gorest.co.in/public-api";
		
		RequestSpecification request = RestAssured.given();
		request.header("Authorization", "Bearer g0JIGvfO6SIYnwmMYDM-Kp3nCYVw-xInSDif");
		
		Response response = request.get("/users");
		
		//AssertThat(response.getBody().jsonPath().get("name"),equalTo("name"));
		
		  // We can convert the Json Response directly into a Java Array by using
		 // JsonPath.getObject method. Here we have to specify that we want to
		 // deserialize the Json into an Array of Book. This can be done by specifying
		 // Book[].class as the second argument to the getObject method.
		
	//	JsonPath  json =response.jsonPath();
	//	json.getObject(path, objectType)
		
		//System.out.println(response.prettyPrint());
		
		/*
		 * System.out.println(" JSON response deserilization"); Result[] results =
		 * response.jsonPath().getObject("_meta", Result[].class);
		 * 
		 * for(Result res: results) { System.out.println(res.getFirst_name()); }
		 */
		
		
		// This way can get the json responses
		
		// How to extract from jsonPath
		
		//response.body().jsonPath().get("name"), hasItem("")
		
	      String name =	response.jsonPath().get("_meta.message").toString();
	       System.out.println(name);
	      
	      String first_name = response.body().jsonPath().get("result[0].first_name").toString();
	      System.out.println(first_name);
	      
	      
	      // it fails here
	      UsersResult result =  response.getBody().as(UsersResult.class);
	      System.out.println(result.getResult());
	      System.out.println("---");
	      
	}
}
