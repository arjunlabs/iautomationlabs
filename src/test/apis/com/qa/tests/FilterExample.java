package com.qa.tests;

import java.io.PrintStream;
import java.io.StringWriter;

import org.apache.commons.io.output.WriterOutputStream;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import static io.restassured.path.xml.XmlPath.*;
import static io.restassured.RestAssured.*; 
import io.restassured.RestAssured;
import io.restassured.filter.log.ErrorLoggingFilter;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.RestAssured;

public class FilterExample {
	
	public static StringWriter requestWriter;
	public static PrintStream requestCapture;
	
	
	
	@BeforeClass
	public static void beforeEach()
	{
		RestAssured.baseURI="http://localhost:8080";
		RestAssured.basePath="/student";
		
	}
	@SuppressWarnings(value = { "" })
	@BeforeTest
	public void init()
	{
		requestWriter = new StringWriter();
		requestCapture = new PrintStream(new WriterOutputStream(requestWriter));
		
	}
	
	@Test
	public void test()
	{
		String responseValue = 
				given()
				.filter(new RequestLoggingFilter())
				.filter(new ResponseLoggingFilter())
		.when().get("/list").toString();
	
		System.err.print(responseValue);
		
	}
	@Test
	public void test2()
	{
	// it will print the output only when the status code lies in the 400 and 500 family
		String responseValue =	given()
				.filter(new ErrorLoggingFilter())
				.when().get("/list").toString();
	
		System.err.print(responseValue);
		
	}

}
