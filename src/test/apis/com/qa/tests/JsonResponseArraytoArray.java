package com.qa.tests;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.testng.annotations.Test;

import com.qa.constants.Endpoints;

import io.restassured.RestAssured;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class JsonResponseArraytoArray {
	
	
	
	@Test
	public void retrieveTheResponseBody()
	{
		RestAssured.baseURI="https://restcountries.eu/rest/v2/alpha";
		
		RequestSpecification  request =	RestAssured.given().log().all();
		
		Response response = request.get(Endpoints.country);
		
		response.then().log().all();
		// 1. 
		// Retrieve the respone of the body
		ResponseBody body = response.getBody();
		
		// By using the ResponseBody.asString() method, we can convert
		// the body into String representation
		
		String  responseBody = body.asString();
		System.out.println("----------------------------");
		System.out.println("Respone Body is: "+ responseBody);
		
		
		// We can convert the Json Response directly into a Java Array by using
		 // JsonPath.getObject method. Here we have to specify that we want to
		 // deserialize the Json into an Array of Book. This can be done by specifying
		 // Book[].class as the second argument to the getObject method.
		
		//2 . get Pretty print
		
		System.out.println(response.prettyPrint());
		
		// Content- Type
		System.out.println(response.getContentType());
		
		// Session - id
		System.out.println(response.getSessionId());
		
		// get the status code
		System.out.println(response.getStatusCode());
		
		// get the status line
		System.out.println(response.getStatusLine());
		
		// get the status code and then print
		int responseCode = response.statusCode();
		System.out.println("Response Code: "+responseCode);


		Headers  header = response.headers();
		
		List<Header> headerValue  = header.asList();
		
		Iterator<Header> itr= headerValue.iterator();
		while(itr.hasNext())
		{
			System.out.println("header value "+ itr.next());
		}
		
		System.out.println(response.time());
		
		System.out.println(response.timeIn(TimeUnit.SECONDS));
		
		
		
	}

}
