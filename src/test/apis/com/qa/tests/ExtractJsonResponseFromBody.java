package com.qa.tests;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class ExtractJsonResponseFromBody {
	
	@Test
	public void getJsonResponse()
	{
RestAssured.baseURI="https://gorest.co.in/public-api";
		
		RequestSpecification request = RestAssured.given();
		request.header("Authorization", "Bearer g0JIGvfO6SIYnwmMYDM-Kp3nCYVw-xInSDif");
		
		Response response = request.get("/users");
		
		   String name =	response.jsonPath().get("_meta.message").toString();
	       System.out.println(name);
	       
	       
	       
	      String first_name = response.body().jsonPath().get("result[0].first_name").toString();
	      System.out.println(first_name);
	      
	     // response.body().jsonPath().getList(result[0].first_name, genericType)
	      
	      for(int i=0 ; i<5 ; i++)
	      {
	    	   String fn = response.body().jsonPath().get("result["+i+"].first_name").toString();
	 	      System.out.println(fn);
	      }
	}

}
