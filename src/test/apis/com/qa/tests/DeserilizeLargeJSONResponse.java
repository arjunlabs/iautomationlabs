package com.qa.tests;

import java.util.Arrays;
import java.util.List;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.labs.deseriliazePojo.GoRest;
import com.labs.deseriliazePojo.Meta;
import com.labs.deseriliazePojo.Result;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
public class DeserilizeLargeJSONResponse {
	
	//https://gorest.co.in/public-api/users?last_name=Moore
	
	public static RequestSpecification request;	
	public static Response response;	
	@BeforeTest
	public void restASsuredExtention()
	{
		RequestSpecBuilder builder = new RequestSpecBuilder();
		builder.setBaseUri("https://gorest.co.in");
		builder.addHeader("Authorization", "Bearer g0JIGvfO6SIYnwmMYDM-Kp3nCYVw-xInSDif");
		RequestSpecification requestSpecification = builder.build();
		
		request = RestAssured.given().spec(requestSpecification);
	}
	
	@Test(priority=1)
	public void deserilizeJsonResponseToPojo()
	{
		request.queryParam("last_name", "Moore");
		response = request.get("/public-api/users");
		System.out.println(response.getBody().prettyPrint());
				
	}

	@Test(priority=2)
	public void validateJsonResponseWithDeserilize()
	{
		
		Result[] resultTest = response.getBody().jsonPath().getObject("Result", Result[].class);
		System.out.println("Test");
		
		List<Result> returnedResults=Arrays.asList(response.getBody().as(Result[].class));
		
		System.out.println(returnedResults.size());
		//System.out.println(resultTest[0].getAddress());
		
		
		System.out.println(resultTest);
		
		//System.out.println(resultTest.getResult());
		//System.out.println(resultTest[0].getFirstName().toString());
	}
	
}
