package com.qa.tests;

import java.io.File;

import org.testng.Assert;
import org.testng.annotations.Test;
import static io.restassured.path.xml.XmlPath.*;
import static io.restassured.RestAssured.*; 
import io.restassured.RestAssured;
import io.restassured.filter.log.ErrorLoggingFilter;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.RestAssured;

public class FileDownloadExample {
	
	
	@Test
	public void test01()
	{
		//1. extract the file size
		
		File inputFile = new File(System.getProperty("user.dir")+File.separator+"\\src\\test\\apis\\com\\qa\\testdata\\geckodriver-v0.26.0-linux32.tar.gz");
	   
		int len = (int) inputFile.length();
		
		System.out.println("Length of the file: "+len);
		
		
		//2
		// Read and download the file
		byte[]  actualValue = given()
		.when()
		.get("https://github.com/mozilla/geckodriver/releases/download/v0.26.0/geckodriver-v0.26.0-linux32.tar.gz")
		.then().extract().asByteArray();
		
		System.out.println("Actual value: "+actualValue.length);
		
		Assert.assertEquals(actualValue.length, len);
	
	}

}
