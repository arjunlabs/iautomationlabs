package com.qa.tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import io.restassured.internal.path.xml.NodeChildrenImpl;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import static io.restassured.RestAssured.*; 
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.testng.Assert.assertTrue;

import org.hamcrest.collection.HasItemInArray;

import static io.restassured.path.xml.XmlPath.*;
import static io.restassured.RestAssured.*; 
import io.restassured.RestAssured;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
public class RequestSpecificationExample {

	// in order to minimize the repetitive code
	static final String API_KEY="75e3u4sgb2khg673cbv2gjup";
	static RequestSpecBuilder builder;
	static RequestSpecification requestSpec;
	
	static ResponseSpecBuilder responseBuilder;
	static ResponseSpecification responseSpec;
	
	
	@BeforeClass
	public void init()
	{
	
		RestAssured.baseURI="https://api.walmartlabs.com";
		RestAssured.basePath="/v1";
	
		builder = new RequestSpecBuilder();
		responseBuilder = new ResponseSpecBuilder();
		builder.addQueryParam("query", "ipod")
		.addQueryParam("format", "json")
		.addParam("apiKey", API_KEY)
		.addParam("facet", "on");
		
		builder.addHeader("Access", "*/*");
		
		requestSpec = builder.build();
		
		
		responseBuilder.expectContentType(ContentType.JSON);
		//responseBuilder.expectHeader("server", "");
		responseBuilder.expectStatusCode(200);
		
		responseBuilder.expectBody("query", equalTo("ipod"));
		responseBuilder.expectBody("runitems", equalTo(10));
		//responseBuilder.expectBody("items.name", hasItem("Refurbished Apple iPod Touch 5th gen 16GB WiFi MP3 MP4 Digital Music Video Player MGG82LL/A"));
		
		responseSpec  = responseBuilder.build();
		
	}
	
	@Test
	public static void test01()
	{
	String queryname= 	given().log().all().spec(requestSpec)
		.when()
		.get("/search")
		.then().extract().body().path("query").toString();
	
	System.out.println(queryname);
	
	}
	@Test
	public static void test02()
	{
	given().spec(requestSpec)
		.when()
		.get("/search")
		.then().spec(responseSpecification);

	
	}
}
