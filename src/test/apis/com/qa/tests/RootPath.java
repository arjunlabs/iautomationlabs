package com.qa.tests;

import java.util.HashMap;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;

import static io.restassured.RestAssured.*; 
import static io.restassured.matcher.RestAssuredMatchers.*;

public class RootPath {
	
	static HashMap<String,String> map= new HashMap<>();
	
	
	@BeforeClass
	public static void init()
	{
		
		RestAssured.baseURI="https://api.walmartlabs.com";
		RestAssured.basePath="";
		
		RestAssured.rootPath="searchresponse.items";  //First way - To give the common root path here which is common to all the response at certain point
		// 2nd way - how to give the root path at the test lever using the root() method
		
	}
	
	@Test
	public void test01()
	{
		HashMap<String, String > queryParam = new HashMap<String,String>();
		queryParam.put("query", "ipod");
		queryParam.put("format", "xml");
		queryParam.put("apiKey", "75e3u4sgb2khg673cbv2gjup");
		
		String name = given().queryParams(queryParam)
		.when()
		.get("/v1/search").then().extract().path("item[0].name").toString();
		
		System.out.println(name);
		
	}
	
	@Test
	public void test01SecondWay()
	{
		HashMap<String, String > queryParam = new HashMap<String,String>();
		queryParam.put("query", "ipod");
		queryParam.put("format", "xml");
		queryParam.put("apiKey", "75e3u4sgb2khg673cbv2gjup");
		
		String name = given().queryParams(queryParam)
		.when()
		.get("/v1/search").then().root("searchresponse.items").extract().path("item[0].name").toString();
		
		System.out.println(name);
		
	}
	
	@AfterClass
	public void tearDown()
	{
		
	}

}
