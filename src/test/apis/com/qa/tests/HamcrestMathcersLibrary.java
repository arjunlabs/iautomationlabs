package com.qa.tests;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.equalToIgnoringCase;
import static org.hamcrest.Matchers.equalToObject;
import static org.hamcrest.Matchers.equalToIgnoringWhiteSpace;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItemInArray;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.hasValue;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import java.util.HashMap;

import org.apache.logging.log4j.core.pattern.EqualsIgnoreCaseReplacementConverter;
import org.testng.annotations.Test;

import io.restassured.RestAssured;

public class HamcrestMathcersLibrary {
	
	@Test
	public void getTheITemAndVerify() {
		RestAssured.baseURI="https://api.walmartlabs.com";
		HashMap<String, String > queryParam = new HashMap<String,String>();
		queryParam.put("query", "ipod");
		queryParam.put("format", "json");
		queryParam.put("apiKey", "75e3u4sgb2khg673cbv2gjup");
		
		given().queryParams(queryParam)
		.when()
		.get("/v1/search").then().body("numItems", equalTo(10));

		
		
		}
	
	// verify query 
	@Test
	public void test01()
	{
		RestAssured.baseURI="https://api.walmartlabs.com";
		HashMap<String, String > queryParam = new HashMap<String,String>();
		queryParam.put("query", "ipod");
		queryParam.put("format", "json");
		queryParam.put("apiKey", "75e3u4sgb2khg673cbv2gjup");
		
		given().queryParams(queryParam)
		.when()
		.get("/v1/search").then().body("query", equalToIgnoringCase("IPOD"));
		
	}
	
	//print
	@Test
	public void test02()
	{
		RestAssured.baseURI="https://api.walmartlabs.com";
		HashMap<String, String > queryParam = new HashMap<String,String>();
		queryParam.put("query", "ipod");
		queryParam.put("format", "json");
		queryParam.put("apiKey", "75e3u4sgb2khg673cbv2gjup");
		
		System.out.println(given().queryParams(queryParam)
		.when()
		.get("/v1/search").then().extract().asString());
		//.body("query", equalToIgnoringCase("IPOD"));
		
	}
	// check single name in arrayList
	@Test
	public void test04()
	{
		RestAssured.baseURI="https://api.walmartlabs.com";
		HashMap<String, String > queryParam = new HashMap<String,String>();
		queryParam.put("query", "ipod");
		queryParam.put("format", "json");
		queryParam.put("apiKey", "75e3u4sgb2khg673cbv2gjup");
		
		given().queryParams(queryParam)
		.when()
		.get("/v1/search").then().body("items.name", hasItem("Apple iPod Touch 16GB A1421 - Space Gray (Refurbished) (5th Generation)"));
		
	}
	// test multiple value in a arraylist
	@Test
	public void test03()
	{
		RestAssured.baseURI="https://api.walmartlabs.com";
		HashMap<String, String > queryParam = new HashMap<String,String>();
		queryParam.put("query", "ipod");
		queryParam.put("format", "json");
		queryParam.put("apiKey", "75e3u4sgb2khg673cbv2gjup");
		
		given().queryParams(queryParam)
		.when()
		.get("/v1/search").then().body("items.name", hasItems("Apple iPod Touch 16GB A1421 - Space Gray (Refurbished) (5th Generation)","Refurbished Apple iPod Touch 16GB MGG82LLA - Space Gray (5th generation)"));
		
	}
	
	// checking values inside maps using hasValue()
	@Test
	public void test05()
	{
		RestAssured.baseURI="https://api.walmartlabs.com";
		HashMap<String, String > queryParam = new HashMap<String,String>();
		queryParam.put("query", "ipod");
		queryParam.put("format", "json");
		queryParam.put("apiKey", "75e3u4sgb2khg673cbv2gjup");
		
		given().queryParams(queryParam)
		.when()
		.get("/v1/search").then().body("items[0].name.giftOptions", hasKey(""));
		
	}
	
	// hasEntry - key value pair
	@Test
	public void test06()
	{
		RestAssured.baseURI="https://api.walmartlabs.com";
		HashMap<String, String > queryParam = new HashMap<String,String>();
		queryParam.put("query", "ipod");
		queryParam.put("format", "json");
		queryParam.put("apiKey", "75e3u4sgb2khg673cbv2gjup");
		
		given().queryParams(queryParam)
		.when()
		.get("/v1/search").then().body("items.findAll{it.name=='Apple iPod touch 7th Generation 256GB - PRODUCT(RED) (New Model)'}", hasItems(hasEntry("name", "Apple iPod touch 7th Generation 256GB - PRODUCT(RED) (New Model)")));
		
	}
	
	//multiple assertion
	@Test
	public void test07()
	{
		RestAssured.baseURI="https://api.walmartlabs.com";
		HashMap<String, String > queryParam = new HashMap<String,String>();
		queryParam.put("query", "ipod");
		queryParam.put("format", "json");
		queryParam.put("apiKey", "75e3u4sgb2khg673cbv2gjup");
		
		given().queryParams(queryParam)
		.when()
		.get("/v1/search").then()
		.statusCode(200).body("items.findAll{it.name=='Apple iPod touch 7th Generation 256GB - PRODUCT(RED) (New Model)'}", hasItems(hasEntry("name", "Apple iPod touch 7th Generation 256GB - PRODUCT(RED) (New Model)")))
		.body("items.name", hasItems("Apple iPod Touch 16GB A1421 - Space Gray (Refurbished) (5th Generation)","Refurbished Apple iPod Touch 16GB MGG82LLA - Space Gray (5th generation)"));
	}
	
	
	// Logical assertion
	
	@Test
	public void test08()
	{
		RestAssured.baseURI="https://api.walmartlabs.com";
		HashMap<String, String > queryParam = new HashMap<String,String>();
		queryParam.put("query", "ipod");
		queryParam.put("format", "json");
		queryParam.put("apiKey", "75e3u4sgb2khg673cbv2gjup");
		
		given().queryParams(queryParam)
		.when()
		.get("/v1/search").then().body("items.size()", equalTo(10))
		.body("items.size()", greaterThan(5))
		.body("items.size()", lessThan(11))
		.body("items.size()", lessThanOrEqualTo(10))
		.body("items.size()", greaterThanOrEqualTo(10));
		
	}
}
