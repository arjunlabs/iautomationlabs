package com.qa.tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import static io.restassured.RestAssured.given;

import java.util.HashMap;

import static io.restassured.RestAssured.*;

public class WalMartExtractReponseTest {
	
	@BeforeClass
	public void init()
	{
		RestAssured.baseURI="https://api.walmartlabs.com";
		
	}
	
	@Test
	public void getIpodDetails()
	{
		HashMap<String,String> queryParam= new HashMap<>();
		
		queryParam.put("query", "ipod");
		queryParam.put("format", "json");
		queryParam.put("apiKey", "75e3u4sgb2khg673cbv2gjup");
		
		
		int runItems = given().queryParams(queryParam)
		.when().get("/v1/search")
		.then().extract().path("numItems");
		System.out.println(runItems);
		
	}

}
