package com.qa.tests;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class ExtractJsonResponseBody {

	
	@Test
	public void displayResponseBody()
	{
        RestAssured.baseURI="https://restcountries.eu/rest/v2/alpha";
		
		RequestSpecification request = RestAssured.given();
		
		Response response = request.get("/IN");
		
		ResponseBody  responseBody = response.getBody();
		
		System.out.println("Response body is: "+ responseBody.asString());
		
	}
	
}
