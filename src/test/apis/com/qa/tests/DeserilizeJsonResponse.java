package com.qa.tests;

import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.labs.pojo.Locations;
import com.labs.pojo.MapsPayload;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class DeserilizeJsonResponse {
	
	// POJO used  from - com.labs.pojo
	// Locations.java
	//MapsPayload.java
	@Test
	public void getJsonResponseMapToPOJO()
	{
		  RestAssured.baseURI="https://rahulshettyacademy.com";
		  
		  RequestSpecification request = RestAssured.given(); 
		  request.queryParam("key","qaclick123");
		  request.queryParam("place_id","5e78e543b0581467b0f436267a2bdd1c");
		  
		  Response response = request.get("/maps/api/place/get/json");
		 
		
		  MapsPayload load =response.getBody().as(MapsPayload.class);
		  
		  System.out.println(load.getAccuracy());
		  System.out.println(load.getAddress());
		  System.out.println(load.getLanguage());
		  System.out.println(load.getName());
		  System.out.println(load.getPhone_number());
		  System.out.println(load.getTypes());
		  System.out.println(load.getWebsite());
		 
		  System.out.println("Deserilization of locations array");
		  Locations loc=  response.jsonPath().getObject("location", Locations.class);
		  System.out.println(loc.getLatitude());
		  System.out.println(loc.getLongitude());
		  
		  System.out.println("Deserilization array");
		  
		/*
		 * Locations[] locat= response.jsonPath().getObject("locations",
		 * Locations[].class); System.out.println(locat.length);
		 * 
		 * for(Locations l:locat) { System.out.println(l.getLatitude());
		 * System.out.println(l.getLongitude());
		 * 
		 * 
		 * }
		 */
		 
		  
		  
		  
		 
		/*
		 * Locations local = new Locations("-98.383494", "73.42736"); MapsPayload
		 * mapPayoad = new MapsPayload(local,"50", "Arjun", "(+91) 983 893 7650",
		 * "Pune Times, KP", "shoe park,shop", "http://railsQAhub.com", "indian-IN");
		 * 
		 * ObjectMapper mapper = new ObjectMapper(); String payload=null; try { payload=
		 * mapper.writerWithDefaultPrettyPrinter().writeValueAsString(mapPayoad); }
		 * catch (JsonProcessingException e) {
		 * 
		 * e.printStackTrace(); }
		 * 
		 * System.out.println(payload);
		 */
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
		  
	}

}
