package com.qa.tests;

import javax.management.DescriptorKey;

import org.testng.annotations.Test;

import com.labs.pojo.StudentList;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Deserlizing_json_Response_To_Pojo {
	
	
	// http://localhost:8080/student/list and its json look like-
	// and run the rest.jar file - execute automation code from youtube series of deserilization
	// This code is working fine

	// this deserilizing is working for array type return- see the response like here- 
	// /LabsAutomation/src/test/apis/com/labs/pojo/DeserlizeJSONBackToPOJO  - find the DeserlizeJSONBackToPOJO here
	
	@Test(priority =1)
	public void getDeseriliizeTest()
	{
		RestAssured.baseURI="http://localhost:8080";
		
		RequestSpecification request= RestAssured.given().log().all();
		
		Response response = request.get("/student/list");
		
		StudentList[] student = response.getBody().as(StudentList[].class);
		
		System.out.println(student[0].getFirstName().toString());
		
	}

	
	@Test(priority =2)
	public void getDeseriliizeTestAndFilter()
	{
		RestAssured.baseURI="http://localhost:8080";
		
		RequestSpecification request= RestAssured.given().log().all();
		
		Response response = request.get("/student/list");
		
		StudentList[] student = response.getBody().as(StudentList[].class);
		
		// filter the courses based on courses
		// like - filter the address based on address
		System.out.println(student[0].getCourses());
		
		// it will print like - 
		// [Accounting, Statistics]
		
	
		
	}
	
	
	@Test(priority =3)
	public void getDeseriliizeTestUsingAnotherMethod()
	{
		RestAssured.baseURI="http://localhost:8080";
		
		RequestSpecification request= RestAssured.given().log().all();
		
		Response response = request.get("/student/list");
		
		StudentList[] student = response.getBody().jsonPath().getObject("StudentList", StudentList[].class);
		
		System.out.println(student[0]); // it is giving null
		//System.out.println(student[0].getFirstName()); // it is giving error
		// filter the courses based on courses
		// like - filter the address based on address
		//System.out.println(student[0].getCourses());
		
		// it will print like - 
		// [Accounting, Statistics]
		
	
		
	}
}
