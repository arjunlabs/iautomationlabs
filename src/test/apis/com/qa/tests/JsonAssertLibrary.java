package com.qa.tests;

import static io.restassured.RestAssured.given;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;

import org.json.JSONException;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.testng.annotations.Test;

import io.restassured.RestAssured;

public class JsonAssertLibrary {
	
	
	@Test
	public void assertTestWithJsonAssert() throws IOException, JSONException
	{
		//		String expected = new String(Files.readAllBytes(Paths.get(System.getProperty("user.dir")+File.separator+"\src\test\apis\com\labs\pojo\ExpectedJSONRespons.txt")));

		String expected = new String(Files.readAllBytes(Paths.get(System.getProperty("user.dir")+File.separator+"\\src\\test\\apis\\com\\labs\\pojo\\ExpectedJSONResponse.txt")));
		HashMap<String, String > queryParam = new HashMap<String,String>();
		queryParam.put("query", "ipod");
		queryParam.put("format", "json");
		queryParam.put("apiKey", "75e3u4sgb2khg673cbv2gjup");
		
		RestAssured.baseURI="https://api.walmartlabs.com";
		String actual =  given().queryParams(queryParam)
				.when()
				.get("/v1/search")
				.then().extract().path("items[0]").toString();
				
		System.out.println(actual);
		
		//JSONAssert.assertEquals(expected,actual, true);		
		JSONAssert.assertEquals(expected,actual, JSONCompareMode.LENIENT);	
		//JSONAssert.assertEquals(expected,actual, JSONCompareMode.STRICT);	
		//JSONAssert.assertEquals(expected,actual, JSONCompareMode.NON_EXTENSIBLE);	
	}

}
