package com.qa.tests;

import java.util.Iterator;
import java.util.List;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class ExtractResponseHeaders {
	
	
	@Test
	public void getTheResponseHeaderAsList()
	{
		
		RestAssured.baseURI="https://restcountries.eu/rest/v2/alpha";
		
		RequestSpecification request = RestAssured.given();
		
		Response response = request.get("/IN");
		
		Headers  header = response.getHeaders();
		
		List<Header> listOfHeaders =  header.asList();
		
		Iterator<Header> itr = listOfHeaders.iterator();
		
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
		
		
	}

}
