package java8featuresConcept;

interface A
{
	void show(int i);
}



 /* class xyz implements A // if you want to remove this block of code then user  annonymos concept 
{
	public void show() {
		System.out.println("Hello");
	}
}*/
 public class LambdaDemo {

	public static void main(String[] args)
	{

		A obj;
		//obj=new xyz();
		
		// AnnonyMous Object
		
		/*obj = new A() {

			public void show() {
				System.out.println("hello");
			
			}
				
		}; */
		
		obj=(int i)-> System.out.println("Hello: "+i); // consumer interface
		obj.show(10);
		
		
	}

}
