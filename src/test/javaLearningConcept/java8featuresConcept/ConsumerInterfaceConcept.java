package java8featuresConcept;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

class consInt implements Consumer<Integer>
{

		public void accept(Integer i) {
		System.out.println(i);
		
	}
	
}
public class ConsumerInterfaceConcept {
	

	public static void main(String[] args) {
		
		List<Integer> values = Arrays.asList(4,5,6,7,8);
	
		Consumer<Integer> c = new consInt();
		values.forEach(c);
		

	}

}
