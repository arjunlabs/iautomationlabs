package java8featuresConcept;

import java.util.Arrays;
import java.util.List;

public class ForEachMethod {
	
	// internal loops - collection internals
	// -> lambda expression

	public static void main(String[] args) {

		
		List<Integer> list= Arrays.asList(4,5,6,7,8,9);
		
		/*
		 * for(int i=0;i<list.size();i++) { System.out.println(list.get(i)); }
		 */
		
		/*
		 * for(int i:list) { System.out.println(i); }
		 */
		
		
	 list.forEach(i -> System.out.println(i));
	}

}
